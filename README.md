# FJ4

A 2.5D physics based puzzler. Play as a marble-sized minibot with an electromagnetic beak and place the copper block in it's slot.

This game was created in a single week for the Friends Jam IV with the goal of learning the Godot game engine. (this being my first project with it)

## Technologies & Tools

- [Godot](https://godotengine.org/)
- [Dialogic](https://dialogic.coppolaemilio.com/)

## Credits

### Fonts

Font|License
-|-
[Kenney's Future Narrow](https://kenney.nl/assets/kenney-fonts)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

### 3D Models

#### CGTrader

Model|Creator|License
-|-|-
[note-papers](https://www.cgtrader.com/free-3d-models/various/various-models/note-papers)|[Earthless188](https://www.cgtrader.com/earthless188)|[CGTrader's Royalty Free License](https://www.cgtrader.com/pages/terms-and-conditions#general-terms-of-licensing)
[generic-eraser](https://www.cgtrader.com/free-3d-models/various/various-models/generic-eraser)|[Satan-s-Dog](https://www.cgtrader.com/satan-s-dog)|[CGTrader's Royalty Free License](https://www.cgtrader.com/pages/terms-and-conditions#general-terms-of-licensing)

#### TurboSquid

Model|Creator|License
-|-|-
[3D PENCIL---Standard Wood](https://www.turbosquid.com/3d-models/3d-pencil-wood-1552568)|[DeepDreamDimension](https://www.turbosquid.com/Search/Artists/DeepDreamDimension)|[TurboSquid's Standard 3D Model License](https://blog.turbosquid.com/turbosquid-3d-model-license/)
[Rulers](https://www.turbosquid.com/3d-models/ruler-rule-max-free/606768)|[visualdestination](https://www.turbosquid.com/Search/Artists/visualdestination)|[TurboSquid's Standard 3D Model License](https://blog.turbosquid.com/turbosquid-3d-model-license/)
[coffee cup 3D model](https://www.turbosquid.com/3d-models/coffee-cup-3d-model-1257598)|[sukruboyraz](https://www.turbosquid.com/Search/Artists/sukruboyraz)|[TurboSquid's Standard 3D Model License](https://blog.turbosquid.com/turbosquid-3d-model-license/)

### PBR Materials

#### ambientCG

Material|License
-|-
[Metal001](ambientCG.com/a/Metal001)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[Metal007](ambientCG.com/a/Metal007)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[Metal021](ambientCG.com/a/Metal021)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[Metal035](ambientCG.com/a/Metal035)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[PaintedMetal004](ambientCG.com/a/PaintedMetal004)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[PavingStones046](ambientCG.com/a/PavingStones046)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[Wood035](ambientCG.com/a/Wood035)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
[Wood063](ambientCG.com/a/Wood063)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

### Audio

#### Music (provided by [Fanatical](https://www.fanatical.com/en/))

Track|Artist|License
-|-|-
Gregoire Lourme|Inside The Asylum|[CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/)
Pinegroove|Blooming Colors|[CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/)
Sun Spot|Awakening (Album Mix)|[CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/)
Adrian Ursu|Spring Reverie|[CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/)

#### Sound Effects

Sound|Creator|License
-|-|-
[Impact-Misc_Tools-0003.wav](https://freesound.org/people/DWOBoyle/sounds/144262/#)|[DWOBoyle](https://freesound.org/people/DWOBoyle/)|[CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
metal shake (remix of [Sounds For Earthquakes - Metal.wav](https://freesound.org/people/RutgerMuller/sounds/51106/#), [Metal water bottle - shaking almost empty bottle](https://freesound.org/people/giddster/sounds/514400/#), [Tab in Can - Shake.wav](https://freesound.org/people/SunnySideSound/sounds/67799/#))|Elatronion (remix), [RutgerMuller](https://freesound.org/people/RutgerMuller/), [giddster](https://freesound.org/people/giddster/), [SunnySideSound](https://freesound.org/people/SunnySideSound/)|[CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
[Clang metallic 2.wav](https://freesound.org/people/deleted_user_2104797/sounds/164561/#)|[deleted_user_2104797](https://freesound.org/people/deleted_user_2104797/)|[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
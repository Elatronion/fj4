extends Spatial
class_name Conduction, "res://resources/custom_nodes/icons/Conduction.svg"

signal conductor_entered
signal conductor_exited

export var area_node_a : NodePath
export var area_node_b : NodePath
export var scene_to_load : Resource

var area_a_conducted : bool = false
var area_b_conducted : bool = false

var conducted : bool = false

var conductive_object : Object = null

func reset():
	area_a_conducted = false
	area_b_conducted = false
	conductive_object = null

func _ready():
	var area_a : Area = get_node(area_node_a)
	var area_b : Area = get_node(area_node_a)
	if !area_a or !area_b:
		return
	area_a.connect("body_entered", self, "_on_AreaA_body_entered")
	area_b.connect("body_entered", self, "_on_AreaB_body_entered")

func _physics_process(delta):
	if Input.is_action_just_pressed("skip_level"):
		if scene_to_load:
				GameManager.load_scene(scene_to_load.resource_path)
	
	var conduction = area_a_conducted and area_b_conducted
	if conducted != conduction:
		if conduction:
			if scene_to_load:
				GameManager.load_scene(scene_to_load.resource_path)
			emit_signal("conductor_entered")
		else:
			emit_signal("conductor_exited")
	conducted = conduction
	if conductive_object != null:
		var distance = conductive_object.global_transform.origin.distance_to(global_transform.origin)
		if distance > 1:
			reset()

func _on_AreaA_body_entered(body):
	area_a_conducted = true
	conductive_object = body

func _on_AreaB_body_entered(body):
	area_b_conducted = true
	conductive_object = body

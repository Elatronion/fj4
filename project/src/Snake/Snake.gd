extends Spatial

onready var rigidbody_head = $RigidBodyHead
onready var player_rigidbody : RigidBody = GameManager.player_rigidbody

export var move_speed = 1.5
export var attack_speed = 200


func attack_player():
	var dir_to_player = rigidbody_head.global_transform.origin.direction_to(player_rigidbody.global_transform.origin).normalized()
	rigidbody_head.apply_central_impulse(dir_to_player * attack_speed)

func move_to_player(delta):
	var dir_to_player = rigidbody_head.global_transform.origin.direction_to(player_rigidbody.global_transform.origin).normalized()
	rigidbody_head.apply_central_impulse(dir_to_player * move_speed)

func _physics_process(delta):
	if player_rigidbody == null:
		return 1
		
	if Input.is_action_just_pressed("snake_attack"):
		attack_player()
	move_to_player(delta)

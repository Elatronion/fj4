extends CanvasLayer

signal transitioned

func _process(delta):
	if Input.is_action_just_pressed("restart"):
		transition()

func transition():
	$AnimationPlayer.play("fade_to_black")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "fade_to_black":
		emit_signal("transitioned")
		$AnimationPlayer.play("fade_to_normal")

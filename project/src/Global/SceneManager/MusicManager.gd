extends Node

var bgm_to_play : String
#var bgm_day : AudioStream = preload("res://assets/audio/1830700-pinegroove-Blooming Colors.ogg")
#var bgm_night : AudioStream = preload("res://assets/audio/1839321-Sun_Spot_(3)-Awakening (Album Mix).ogg")

func _ready():
	GameManager.connect("load_bgm", self, "_on_load_bgm")

func _on_load_bgm(bgm_str):
	if bgm_to_play != bgm_str:
		bgm_to_play = bgm_str
		transition()

func transition():
	$AnimationPlayer.play("audio_fade_out")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "audio_fade_out":
		var bgm = load(bgm_to_play)
		$AudioStreamPlayer.stop()
		$AudioStreamPlayer.stream = bgm
		$AudioStreamPlayer.play()
		$AnimationPlayer.play("audio_fade_in")

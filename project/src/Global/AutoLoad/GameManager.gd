extends Node

signal load_scene(scene_str)
signal load_bgm(bgm_str)

var player_rigidbody : RigidBody

func set_player_rigidbody(rb : RigidBody):
	player_rigidbody = rb

func dialogue_load_scene(scene_str_arr):
	load_scene(scene_str_arr[0])

func load_scene(scene_str : String):
	emit_signal("load_scene", scene_str)

func dialogue_load_bgm(bgm_str_arr):
	load_bgm(bgm_str_arr[0])

func load_bgm(bgm_str : String):
	emit_signal("load_bgm", bgm_str)

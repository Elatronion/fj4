extends Spatial

onready var body = $Body
onready var foot = $LLeg1/LegBottom

func _physics_process(delta):
	body.apply_central_impulse(Vector3(0, 1.5, 0))
	foot.apply_central_impulse(Vector3(0, -1, 0))

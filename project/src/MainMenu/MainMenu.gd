extends Control

func _ready():
	$VBoxContainer/StartButton.grab_focus()


func _on_StartButton_pressed():
	GameManager.load_scene("res://scenes/DayCutscene.tscn")
	GameManager.load_bgm("res://assets/audio/1830700-pinegroove-Blooming Colors.ogg")


func _on_QuitButton_pressed():
	get_tree().quit()

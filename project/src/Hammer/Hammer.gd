extends Spatial

onready var hammer_arm = $HammerCombiner/HammerArm
onready var hammer_head = $HammerCombiner/HammerHead

func _on_ActivationArea_body_entered(body):
	hammer_arm.sleeping = false
	hammer_head.sleeping = false
